import express from "express";

/* Routes */
import { loadApiEndpoints } from "./Routes/status";
import { HelloWorldRoutes } from "./Modules/HelloWorld/infrastructure/Routes/HelloWorldRoutes";

var cors = require('cors');
// Create Express server
const app = express();
// Express configuration
app.set("port", process.env.PORT || 4444);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(cors());

// Load API routes 
loadApiEndpoints(app);
HelloWorldRoutes(app);

export default app;
