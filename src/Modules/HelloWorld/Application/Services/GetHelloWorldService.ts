import { Request } from 'express';
import { InternalResponseInterface } from '../../../../Shared/DTO/Responses/InternalResponseInterface';
import { HelloWorldRepository } from '../../infrastructure/Repository/HelloWorldRepository';

export class GetHelloWorldService {
    
    private repository;

    constructor(HelloWorldRepository : HelloWorldRepository) 
    {
        this.repository = HelloWorldRepository;
    }

    getMessage() : InternalResponseInterface
    {
        return this.repository.getMessage();
    }
}