import { StatusCode, Success } from '../../../../Shared/Dictionaty';
import { InternalResponseInterface } from '../../../../Shared/DTO/Responses/InternalResponseInterface';
import { ApiResponseInterface } from '../../../../Shared/DTO/Responses/ApiResponseInterface';
import { Application, Request, Response } from "express";

/* Controllers */

import { GetHelloWorldController } from '../Controllers/GetHelloWorldController';
const getHelloWorldController = new GetHelloWorldController();

/* End of Controllers */

const apiVersion = 'v1';
 
export const HelloWorldRoutes = (app: Application): void => {

    app.get(`/api/${apiVersion}/helloworld/`, async (req: Request, res: Response) => {
        const helloworldMessage : InternalResponseInterface = getHelloWorldController.getMessage(req, res);
        if (!helloworldMessage.success){
            const response : ApiResponseInterface = {
                message : '',
                success : Success.KO,
                data : {},
            };
            return res.status(StatusCode.INTERNAL_SERVER_ERROR).json(response);
        }else{
            const response : ApiResponseInterface = {
                message : '',
                success : Success.OK,
                data : {
                    message : helloworldMessage.message
                },
            };
            return res.status(StatusCode.OK).json(response);
        }
    });
};

  