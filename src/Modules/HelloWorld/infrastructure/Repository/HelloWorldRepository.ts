import { Messages } from "../../../../Shared/Translates/en/Messages";
import { InternalResponseInterface } from "../../../../Shared/DTO/Responses/InternalResponseInterface";

export class HelloWorldRepository 
{
    getMessage() : InternalResponseInterface
    {
        return {
            success : true,
            message : Messages.INITIAL_MESSAGE,
        }
    }
}