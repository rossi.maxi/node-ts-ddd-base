import { Request, Response } from 'express';
import { InternalResponseInterface } from '../../../../Shared/DTO/Responses/InternalResponseInterface';
import { GetHelloWorldService } from '../../Application/Services/GetHelloWorldService';
import { HelloWorldRepository } from '../Repository/HelloWorldRepository';

export class GetHelloWorldController {

    private service;

    constructor() 
    {
        this.service = new GetHelloWorldService(new HelloWorldRepository());
    }

    getMessage = (req: Request, res: Response) : InternalResponseInterface =>
    {
       return this.service.getMessage();
    }

}