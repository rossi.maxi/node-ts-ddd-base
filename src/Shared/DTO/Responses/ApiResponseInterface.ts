export interface ApiResponseInterface {
    success : Boolean,
    message? : String,
    errCode? : String,
    data? : Object,
}

