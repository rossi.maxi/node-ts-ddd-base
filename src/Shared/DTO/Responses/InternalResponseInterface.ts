export interface InternalResponseInterface {
    success : boolean;
    message? : string;
    data? : object;
}
