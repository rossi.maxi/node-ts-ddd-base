export const Messages = {
    ERR_INPUT_DATA  : 'Error in input data',
    ERR_INTERNAL_ERROR : 'Internal Error',
    ERR_SERVER_ERROR : 'Server Error',
    ERR_GENERAL : 'Error',
    SUCCESS : 'Success'
}

export const StatusCode = {
    OK : 200,
    BAD_REQUEST : 400,
    FORBIDDEN : 403,
    INTERNAL_SERVER_ERROR : 500,
    // ...
}

export const Success = {
    OK : true,
    KO : false,
}

export const Tables = {
  // ...
}





