import { ValueObject } from "../Interfaces/ValueObject";
import { z } from "zod"; 
// ZOD DOCUMENTATION
// https://github.com/colinhacks/zod

export class GeneralValidator {

    isEmail(email:string) : ValueObject {
        return {
            isValid : z.string().email().isEmail,
            value : email,
        }
    }
}