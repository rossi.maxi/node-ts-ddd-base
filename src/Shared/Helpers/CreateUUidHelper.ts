import { v4 as uuid } from 'uuid';

export const generateUuid = () : string => {
    const id: string = uuid();
    return id;
}
