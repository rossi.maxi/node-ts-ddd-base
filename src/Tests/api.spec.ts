import request from "supertest";

import app from "../app";

describe("GET /status", () => {
  it("should return 200 OK", () => {
    return request(app).get("/status").expect(200);
  });
});
