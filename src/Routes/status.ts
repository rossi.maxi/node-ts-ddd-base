import { Application, Request, Response } from "express";

/* STATUS ENDPOINT */

export const loadApiEndpoints = (app: Application): void => {
  app.get("/status", (req: Request, res: Response) => {
    return res.status(200).send({
      success: true,
    });
  });
};
